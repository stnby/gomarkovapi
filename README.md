# Markov Chain API

A simple API written in Golang and powered by Redis DB\
that generates 2 ended Markov chain.

Used as a chatbot for my Telegram group / kChat

## Usage

```sh
# Check if Application and DB are functional.
curl -s 'http://localhost:8080/alive' | jq
{
  "alive": true
}
```

```sh
# Train model with our input (default model is 'default').
curl -s -G 'http://localhost:8080/' --data-urlencode "text=Hello, World!" | jq
{
  "input": "Hello, World!",
  "model": "default",
  "output": "",
  "seed": "World!"
}
```

```sh
# Lets try my own trained model. (training disabled).
curl -s -G 'http://localhost:8080/?model=kernal&train=false' --data-urlencode "text=But hey at least golang doesn't have ;" | jq
{
  "input": "But hey at least golang doesn't have ;",
  "model": "kernal",
  "output": "mega ton more indian IT WAS SOOOOO FREAKIN KAWAII DESU ; compare this too bb",
  "seed": ";"
}
```

```sh
# Can also enable quiet mode (for learning only).
curl -s -G 'http://localhost:8080/?model=kernal&quiet=true' --data-urlencode "text=omfg my IQ is dropping rapidly" | jq
{
  "input": "omfg my IQ is dropping rapidly",
  "model": "kernal"
}
```

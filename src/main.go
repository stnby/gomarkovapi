package main

import (
	"context"
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
)

type ChainKey struct {
	Prev string `json:"prev"`
	Next string `json:"next"`
}

var maxNext int = 10
var maxPrev int = 10
var maxRetry int = 10

var ctx = context.Background()
var redisClient *redis.Client

/* Helper function to handle environmental variables. */
func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

/* Check if Redis DB is up. */
func HealthHandler(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json; charset=utf-8")
	res.Header().Set("Access-Control-Allow-Origin", "*")
	alive := true
	err := redisClient.Ping(ctx).Err()
	if err != nil {
		alive = false
	}
	json.NewEncoder(res).Encode(map[string]bool{"alive": alive})
}

/* Return a random link of a chain for a requested word in a model. */
func QueryChainWord(model, word string) (ChainKey, error) {
	var keyStruct ChainKey
	key := model + "_" + word

	redisRes, err := redisClient.SRandMember(ctx, key).Result()
	if err != nil {
		return keyStruct, err
	}

	err = json.Unmarshal([]byte(redisRes), &keyStruct)
	if err != nil {
		return keyStruct, err
	}

	return keyStruct, nil
}

/* Feed dat chain with data. :) */
func FeedChain(tokens []string, model string) {
	var keyStruct ChainKey
	tokensLength := len(tokens) - 1
	for index := range tokens {
		keyStruct.Prev = ""
		if index > 0 {
			keyStruct.Prev = tokens[index - 1]
		}

		keyStruct.Next = ""
		if index < tokensLength {
			keyStruct.Next = tokens[index + 1]
		}

		jsonKey, err := json.Marshal(keyStruct)
		if err != nil {
			panic(err)
		}

		key := model + "_" + tokens[index]
		redisClient.SAdd(ctx, key, string(jsonKey))
	}
}

/* Generate a chain from a given seed from selected model. */
func GenerateChain(seed string, model string) (string, error) {
	chainArr := []string{}

	tmpToken := seed
	for i := 0; i < maxNext; i++ {
		keyStruct, err := QueryChainWord(model, tmpToken)
		if err != nil {
			return "", err
		}

		chainArr = append(chainArr, tmpToken)

		tmpToken = keyStruct.Next
		if tmpToken == "" {
			break
		}
	}

	tmpToken = seed
	for i := 0; i < maxPrev; i++ {
		keyStruct, err := QueryChainWord(model, tmpToken)
		if err != nil {
			return "", err
		}

		tmpToken = keyStruct.Prev
		if tmpToken == "" {
			break
		}

		chainArr = append([]string{tmpToken}, chainArr...)
	}
	return strings.Join(chainArr, " "), nil
}

func ChainHandler(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json; charset=utf-8")
	res.Header().Set("Access-Control-Allow-Origin", "*")

	query := req.URL.Query()
	textQuery, _ := query["text"]

	if len(textQuery) < 1 || textQuery[0] == "" {
		code := http.StatusBadRequest
		res.WriteHeader(code)
		json.NewEncoder(res).Encode(map[string]interface{}{
			"error": map[string]interface{}{
				"code": code,
				"message": http.StatusText(code),
			},
		})
		return
	}
	text := textQuery[0]

	model := "default"
	modelQuery, _ := query["model"]
	if len(modelQuery) > 0 && modelQuery[0] != "" {
		model = modelQuery[0]
	}

	tokens := regexp.MustCompile("\\s+").Split(strings.TrimSpace(text), -1)

	if tokens[0] == "" {
		code := http.StatusNotAcceptable
		res.WriteHeader(code)
		json.NewEncoder(res).Encode(map[string]interface{}{
			"error": map[string]interface{}{
				"code": code,
				"message": http.StatusText(code),
			},
			"input": text,
		})
		return
	}

	// Train the chain (if training is not disabled).
	trainQuery, _ := query["train"]
	if len(trainQuery) < 1 || trainQuery[0] != "false" {
		FeedChain(tokens, model)
	} else {
		key := model + "_" + tokens[0]
		redisRes, err := redisClient.Exists(ctx, key).Result()
		if err != nil || redisRes == 0 {
			code := http.StatusNotAcceptable
			res.WriteHeader(code)
			json.NewEncoder(res).Encode(map[string]interface{}{
				"error": map[string]interface{}{
					"code": code,
					"message": http.StatusText(code),
				},
				"model": model,
			})
			return
		}
	}

	// Generate response (if not in quiet mode).
	var seed, output string
	quietQuery, _ := query["quiet"]
	if len(quietQuery) < 1 || quietQuery[0] != "true" {
		for i := 0; i < maxRetry; i++ {
			seed = tokens[rand.Intn(len(tokens))]
			generated, err := GenerateChain(seed, model)
			if err != nil {
				code := http.StatusInternalServerError
				res.WriteHeader(code)
				json.NewEncoder(res).Encode(map[string]interface{}{
					"error": map[string]interface{}{
						"code": code,
						"message": err,
					},
					"model": model,
					"seed": seed,
					"text": text,
				})
				return
			}
			output = generated
			if output != generated {
				break
			}
		}
		json.NewEncoder(res).Encode(map[string]interface{}{
			"input": text,
			"output": output,
			"model": model,
			//"tokens": tokens,
			"seed": seed,
		})
	} else {
		json.NewEncoder(res).Encode(map[string]interface{}{
			"input": text,
			"model": model,
		})
	}
}

func main() {
	redisClient = redis.NewClient(&redis.Options{
		Addr: getenv("REDIS_HOST", "localhost:6379"),
	})

	router := http.NewServeMux()
	router.HandleFunc("/alive", HealthHandler)
	router.HandleFunc("/", ChainHandler)

	srv := &http.Server {
		Handler: router,
		Addr:    ":" + getenv("LISTEN_PORT", "8080"),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
